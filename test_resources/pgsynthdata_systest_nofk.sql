--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6
-- Dumped by pg_dump version 12.6

-- Started on 2021-12-03 16:36:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 140775)
-- Name: no_fk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.no_fk (
    nofk1 integer,
    nofk2 text
);


ALTER TABLE public.no_fk OWNER TO postgres;

--
-- TOC entry 2812 (class 0 OID 140775)
-- Dependencies: 202
-- Data for Name: no_fk; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.no_fk (nofk1, nofk2) FROM stdin;
99	bla
99	bla
99	bla
99	bla
1	bli
\.


-- Completed on 2021-12-03 16:36:02

--
-- PostgreSQL database dump complete
--

