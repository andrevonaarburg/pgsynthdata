--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6
-- Dumped by pg_dump version 12.6

-- Started on 2021-12-03 16:34:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 140831)
-- Name: empty_rows; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empty_rows (
    id integer NOT NULL,
    empty_row1 integer,
    empty_row2 date,
    empty_row3 text
);


ALTER TABLE public.empty_rows OWNER TO postgres;

--
-- TOC entry 2814 (class 0 OID 140831)
-- Dependencies: 202
-- Data for Name: empty_rows; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empty_rows (id, empty_row1, empty_row2, empty_row3) FROM stdin;
\.


--
-- TOC entry 2687 (class 2606 OID 140838)
-- Name: empty_rows empty_rows_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empty_rows
    ADD CONSTRAINT empty_rows_pkey PRIMARY KEY (id);


-- Completed on 2021-12-03 16:34:45

--
-- PostgreSQL database dump complete
--

