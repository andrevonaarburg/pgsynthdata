Pure Synthetic Data Generation for PostgreSQL
=============================================

Description
-----------

``pgsynthdata`` is a lightweight tool for PostgreSQL written in Python.
It can generate purely synthetic data from existing databases that have
the same statistical properties as the original data. The result is a
generated database with the same structure as the original, but with
synthetic data.

The synthetic data is generated using the information provided by the
`pg_stats <https://www.postgresql.org/docs/current/view-pg-stats.html>`__
view. More explicitly by using statistical information such as, most
common values, their frequencies, histogram, fraction of nulls, the
number of distinct values etc… From this information, the generators
create a statistical model that produces fully synthetic data that do
not contain actual values or fragments of the “real” data but are very
similar in terms of “shape” and their properties.

All this is done without any cumbersome configuration. However, several
specialized generators can be used with very little configuration effort
to generate even more realistic data, e.g. person names, street names or
postal codes by using ``column comments``.

`GitLab Repo <https://gitlab.com/geometalab/pgsynthdata>`__

Installation
------------

Prerequisite
~~~~~~~~~~~~

Make sure you have these two components ready for this tool to work:

1. You must have the `PostgreSQL Client
   Applications <https://www.postgresql.org/docs/current/reference-client.html>`__
   **localy** installed > You can use the console command
   ``pg_dump --version`` and ``pg_restore --version`` to check if the
   needed utilities are installed
2. You should have a database for which you want to generate data. The
   database can exist locally or be accessible via a PostgreSQL docker
   container. > Don’t have a database available? Look at the
   Test database section. There you will find an example of how
   to set up a test database with Docker.

Stable version from PyPi
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   pip install pgsynthdata

After installation, you can run the program from your console. See
chapter `usage <#usage>`__ for more information

Constraints
-----------

There are some limitations regarding the database structure and
supported data types.

1. Currently these PostgreSQL types are supported for columns which do
   not have any external constraints

-  `Nummeric: <https://www.postgresql.org/docs/current/datatype-numeric.html>`__
   ``integer``, ``smallint``, ``bigint``, ``numeric``,
   ``double precision``, ``real``
-  `Character: <https://www.postgresql.org/docs/current/datatype-character.html>`__
   ``text``, ``character varying``, ``character``
-  `Date/Time: <https://www.postgresql.org/docs/current/datatype-datetime.html>`__
   ``date``, ``timestamp with time zone``,
   ``timestamp without time zone``
-  `Binary: <https://www.postgresql.org/docs/current/datatype-binary.html>`__
   ``bytea``
-  `Boolean: <https://www.postgresql.org/docs/current/datatype-boolean.html>`__
   ``boolean``
-  `Enumerated: <https://www.postgresql.org/docs/current/datatype-enum.html>`__
-  `Geometric: <https://www.postgresql.org/docs/current/datatype-geometric.html>`__
   ``point``, ``line``, ``polygon``, ``circle``, ``box``, ``lseg``,
   ``path``
-  `(Nummeric:)
   Arrays <https://www.postgresql.org/docs/current/arrays.html>`__
   ``integer[]``, ``smallint[]``, ``bigint[]``, ``numeric[]``,
   ``double precision[]``, ``real[]``
-  `Spatial (PostGIS): <https://postgis.net>`__ ``ST_Point``

2. Data with **foreign key** constraints can only be of *Numeric*,
   *Character* or *Datetime* type (and its subtypes and variations)
3. Currently, **composite unique keys** are not supported. Also, no
   tables with **self-references** are supported

Usage
-----

.. code:: sh

   pgsynthdata [GENERAL_OPTIONS] SOURCE_DATABASE COMMAND [COMMAND_OPTIONS]

The first thing the tool does after each command is to ask you for the
database password. This will never be stored or logged on the console or
anywhere else.

General Options
~~~~~~~~~~~~~~~

These options are valid for all commands

+---------+------------------------------------------------------------+
| General | Description                                                |
| Option  |                                                            |
+=========+============================================================+
| ``-h/-  | Hostname of the Database (default: localhost)              |
| -host`` |                                                            |
+---------+------------------------------------------------------------+
| ``-p/-  | Port of the Database (default: 5432)                       |
| -port`` |                                                            |
+---------+------------------------------------------------------------+
| ``-u/-  | User of the Database (default: postgres)                   |
| -user`` |                                                            |
+---------+------------------------------------------------------------+
| `       | Name(s) of table(s) to be filled with data, separated by   |
| `-t/--t | ``:`` (default: all tables)                                |
| ables`` |                                                            |
+---------+------------------------------------------------------------+
| ``      | Turn on verbose mode                                       |
| -v/--ve |                                                            |
| rbose`` |                                                            |
+---------+------------------------------------------------------------+

Commands
~~~~~~~~

There are three commands, which are explained below

Generate
^^^^^^^^

Generate is the main command. It generates a database with the same
structure but purely synthetic data from an original database.

.. code:: sh

   pgsynthdata [GENERAL_OPTIONS] SOURCE_DATABASE generate DEST_DATABASE [COMMAND_OPTIONS]

+------------+---------------------------------------------------------+
| Command    | Description                                             |
| Option     |                                                         |
+============+=========================================================+
| `          | Multiplication factor for the number of lines to be     |
| `-m/--m_fa | generated (default: 1.0)                                |
| ctor``\ \* |                                                         |
+------------+---------------------------------------------------------+
| ``-o       | Owner of the created destination database               |
| /--owner`` |                                                         |
+------------+---------------------------------------------------------+
| ``-        | Drop the existing destination databases first (default: |
| d/--drop`` | false)                                                  |
+------------+---------------------------------------------------------+

..

   \*Remarks for ``-m/--m_factor``: This feature is experimental and
   there are many reasons this can fail, e.g., when unique data must be
   generated but the range is smaller than the number of values to
   generate

Here are some examples:

.. code:: sh

   pgsynthdata mydb generate mydb_gen
   pgsynthdata -t "table_a:table_b:table_c" mydb generate mydb_gen --drop
   pgsynthdata -u testuser -p 1234 --verbose mydb generate mydb_gen -d -m 2.0 --owner testuser2

Show
^^^^

Show can be used to display the structure of a database. It shows
relevant information for the database, such as the data type, whether it
is a foreign key/nullable/unique, which generator is used, and column
comments. This is especially useful before generation. There are no
‘COMMAND_OPTIONS’.

.. code:: sh

   pgsynthdata [GENERAL_OPTIONS] SOURCE_DATABASE show

Here are some examples:

.. code:: sh

   pgsynthdata mydb show
   pgsynthdata -u testuser mydb show

Analyze
^^^^^^^

Analyze compares the original database with the generated database in
terms of statistical key properties. It retrieves the relevant
statistical information for the databases from ``pg_stats`` and saves it
as either HTML or JSON. The file will be saved in the current working
directory as ``Analyze ORIGINAL_DATABASE GENERATED_DATABASE.html/json``.
The original and generated column will be adjacent to allow easier
comparison of the values. This is useful for validation after
generation.

.. code:: sh

   pgsynthdata [GENERAL_OPTIONS] ORIGINAL_DATABASE analyze GENERATED_DATABASE [COMMAND_OPTIONS]

=========================== =============================
Command Option              Description
=========================== =============================
``-f/--format {html/json}`` Output format (default: html)
=========================== =============================

Here are some examples:

.. code:: sh

   pgsynthdata mydb analyze mydb_gen
   pgsynthdata mydb analyze mydb_gen --format=json

Configuration for specific Data Generators
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are several specific data generators. For the tool to use these
generators, a `SQL
comment <https://www.postgresql.org/docs/9.1/sql-comment.html>`__ must
be present at this column.

For example, there is one that can generate real first names. The
generator has the identifier ``PERSON_FIRST_NAME``. To use it, the
comment in that column must contain ``PGSYNTHDATA[PERSON_FIRST_NAME]``
(including the square brackets). Any other characters may be placed
before and after ``blah blah PGSYNTHDATA[PERSON_FIRST_NAME] blah blah``.

You can easily enter comments via pgAdmin, or as SQL command. More
information about PostgreSQL comments can be found
`here <https://www.postgresql.org/docs/9.1/sql-comment.html>`__.

.. code:: postgres

   COMMENT ON COLUMN test_table.name IS 'PGSYNTHDATA[PERSON_FIRST_NAME]';

Until now, these specific generators are available:

+------------------------+--------------------------------------------+
| Generator identifier   | Description                                |
+========================+============================================+
| PERSON_FIRST_NAME      | Generates real first names, e.g. “Rick”    |
+------------------------+--------------------------------------------+
| PERSON_LAST_NAME       | Generates real last names, e.g. “Sanchez”  |
+------------------------+--------------------------------------------+
| PERSON_NAME            | Generates real full names, e.g. “Rick      |
|                        | Sanchez”                                   |
+------------------------+--------------------------------------------+
| PERSON_PREFIX          | Generates real name prefixes               |
+------------------------+--------------------------------------------+
| PERSON_SUFFIX          | Generates real full suffixes               |
+------------------------+--------------------------------------------+
| ADDRESS_STREET         | Generates real street names,               |
|                        | e.g. “Oberseestrasse”                      |
+------------------------+--------------------------------------------+
| ADDRESS_ZIP            | Generates real street zips, e.g. “8640”    |
+------------------------+--------------------------------------------+
| ADDRESS_BUILDING_NR    | Generates real building numbers, e.g. “10” |
+------------------------+--------------------------------------------+
| ADDRESS_CITY           | Generates real city names,                 |
|                        | e.g. “Rapperswil”                          |
+------------------------+--------------------------------------------+
| ADDRESS_STREET_ADDRESS | Generates real addresses,                  |
|                        | e.g. “Oberseestrasse 10”                   |
+------------------------+--------------------------------------------+
| ADDRESS_COUNTRY        | Generates real country names,              |
|                        | e.g. “Schweiz”                             |
+------------------------+--------------------------------------------+
| ADDRESS_COUNTRY_CODE   | Generates real country codes, e.g. “CH”    |
+------------------------+--------------------------------------------+
| COPY                   | The original data is taken one-to-one (but |
|                        | shuffled)                                  |
+------------------------+--------------------------------------------+
| IGNORE                 | No data generated (filled with the default |
|                        | type, most likely ``NUll``)                |
+------------------------+--------------------------------------------+

Test database
~~~~~~~~~~~~~

The tests attempt to connect to a test database.

A SQL dump of the ``pgsynthdata_testdb`` database can be found in
``./pgsynthdata/test_resources`` folder. This database should be used to
run the unit tests. You can import the database via pgAdmin or by using
these PostgreSQL CLI commands:

.. code:: sh

   psql -U postgres -h localhost -p 5432 -c "create database pgsynthdata_testdb"
   psql -U postgres -h localhost -p 5432 -v ON_ERROR_STOP=on -d "pgsynthdata_testdb" -f "mydb/pgsynthdata_testdb.sql"

To spin up the test database, you can use the following docker command:

.. code:: docker

   docker run --rm -e POSTGRES_DB=pgsynthdata_testdb -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_HOST_AUTH_METHOD=trust -p "5432:5432" postgres:14

Authors
-------

-  `Timon Erhart <https://gitlab.com/turbotimon>`__
-  `Jari Elmer <https://gitlab.com/jarielmer>`__

Based on preliminary work by `Etienne
Baumgartner <https://gitlab.com/EPB1996>`__, `Kevin
Ammann <https://gitlab.com/kevinost>`__ and `Labian
Gashi <https://gitlab.com/labiangashi>`__.
