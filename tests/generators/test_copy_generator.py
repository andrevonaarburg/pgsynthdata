import pytest

from pgsynthdata.generators.copy_generator import copy_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
GENERATOR = copy_generator


def test_generate_testdb():
    testtable = TESTDB_TABLES[GENERATOR.__name__]
    source_no_of_columns = len(testtable.columns)
    generated_columns = 0
    for colname, col in testtable.columns.items():
        generated = GENERATOR(
            col.stats, col.pg_info, col.constraints, testtable.rows_to_generate
        )
        assert (
            generated != None
        ), f"no data generated ({testtable.pg_info.name}.{colname})"
        assert (
            len(generated) == testtable.rows_to_generate
        ), f"generated length is {len(generated)} / expected {testtable.rows_to_generate} ({testtable.pg_info.name}.{colname})"
        generated_columns += 1
    assert (
        source_no_of_columns == generated_columns
    ), f"generated columns {generated_columns} / expected {source_no_of_columns} ({testtable.pg_info.name})"
