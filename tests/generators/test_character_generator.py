import pytest

from pgsynthdata.generators.character_generator import character_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
DATA_TYPES = {"text", "character varying", "character"}
GENERATOR = character_generator
EXPECTED_TYPES = str


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR)


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)


def test_fallbacks():
    stats = helpers.empty_statistics()
    stats.pg_stats.avg_width = 12
    col_info = helpers.empty_column_pg_infos()
    col_info.character_maximum_length = 10
    helpers.fallbacks(
        DATA_TYPES, GENERATOR, EXPECTED_TYPES, stats=stats, column_info=col_info
    )


# def test_fallback():
#     stats = empty_Statistics()
#     stats.pg_stats.avg_width = 12
#     col_info = empty_ColumnPgInfos()
#     col_info.character_maximum_length = 24


#     ROWS_TO_GEN = 10

#     generated = character_generator(stats, col_info, constraints, ROWS_TO_GEN)

#     assert len(generated) == ROWS_TO_GEN
#     assert isinstance(generated[0], str)
