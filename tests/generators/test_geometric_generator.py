from pgsynthdata.datagenerator import AsIs
from pgsynthdata.generators.geometric_generator import geometric_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
DATA_TYPES = {"point", "line", "polygon", "circle", "box", "lseg", "path"}
GENERATOR = geometric_generator
EXPECTED_TYPES = AsIs


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR)


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)


def test_fallbacks():
    helpers.fallbacks(DATA_TYPES, GENERATOR, EXPECTED_TYPES)
