from datetime import date, datetime

from pgsynthdata.generators.datetime_generator import datetime_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()

DATA_TYPES = {"date", "timestamp without time zone"}
GENERATOR = datetime_generator
EXPECTED_TYPES = (date, datetime)


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR)


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)


def test_fallbacks():
    helpers.fallbacks(DATA_TYPES, GENERATOR, EXPECTED_TYPES)
