from pgsynthdata.datagenerator import Statistics, empty_statistics
from pgsynthdata.generators.array_generator_utils import generate_array_lengths


def test_generate_array_lengths():
    stats: Statistics = empty_statistics()
    stats.pg_stats.elem_count_histogram = [
        4,
        4,
        4,
        4,
        5,
        5,
        5,
        7,
        7,
        7,
        7,
        10,
        10,
        10,
        11,
        11,
        12,
        13,
        13,
        17,
        35,
        35,
        36,
        36,
        37,
        37,
        40,
        46,
        54,
        16.123,
    ]
    rows_to_gen = 1000
    elements = generate_array_lengths(stats, rows_to_gen)
    assert len(elements) == rows_to_gen
