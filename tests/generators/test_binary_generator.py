from pgsynthdata.generators.binary_generator import binary_generator

from .. import testdb_pickler
from . import testhelpers as helpers

TESTDB_TABLES = testdb_pickler.unpickle_db()
DATA_TYPES = {"bytea"}
GENERATOR = binary_generator
EXPECTED_TYPES = str


def test_registration():
    helpers.registration(DATA_TYPES, GENERATOR)


def test_generate_testdb():
    helpers.testdb(TESTDB_TABLES, DATA_TYPES, GENERATOR, EXPECTED_TYPES)


def test_fallback():
    helpers.fallbacks(DATA_TYPES, GENERATOR, EXPECTED_TYPES)
