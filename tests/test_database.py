import os
import uuid

import psycopg2
import pytest

from pgsynthdata.database import *
from pgsynthdata.database import _create_insert_query

from .testhelpers import DBNAME, HAS_POSTGRES_CLIENT_TOOLS, HOST, PORT, PW, USER


@pytest.fixture(scope="session")
def test_dump_file(tmp_path_factory):
    return tmp_path_factory.mktemp("dump_files") / "test_dump_database"


@pytest.fixture
def database() -> Database:
    database = None
    try:
        database = Database(DBNAME, USER, PW, HOST, PORT)
    except:
        pytest.skip("can not connect to database")
    return database


@pytest.fixture
def table(database: Database) -> TablePgInfo:
    tables = database.get_tables()
    if len(tables) == 0:
        pytest.skip("no tables to get stats from")
    return tables[0]


@pytest.fixture
def column(database: Database, table: TablePgInfo) -> ColumnPgInfo:
    column_infos = database.get_column_infos(table.name)
    if len(column_infos) == 0:
        pytest.skip(f"no columns in table {table.name} to get stats from")
    return column_infos[0]


def create_random_db_name():
    return f"db_{uuid.uuid1()}".replace("-", "")


def test_database_connection():
    try:
        conn = psycopg2.connect(
            dbname=DBNAME, user=USER, password=PW, host=HOST, port=PORT
        )
        # closed is 0 if connected
        assert conn.closed == 0
        conn.close()
        # closed is non 0 if connection is closed
        assert conn.closed != 0
    except Exception as ex:
        pytest.fail("Could open connection to the test-DB")


def test_creation():
    try:
        db = Database(DBNAME, USER, PW, HOST, PORT)
    except Exception as ex:
        pytest.fail("Could open create Database class")


def test_connection_url(database: Database):
    assert (
        database.get_connection_url()
        == f"postgresql://{USER}:{PW}@{HOST}:{PORT}/{DBNAME}"
    )


def test_db_exists(database: Database):
    assert database.does_db_exist(create_random_db_name()) is False
    assert database.does_db_exist(DBNAME) is True


def test_create_and_drop_database(database: Database):
    newDbName = create_random_db_name()
    database.create_database(newDbName)
    assert database.does_db_exist(newDbName) is True
    database.drop_database(newDbName)
    assert database.does_db_exist(newDbName) is False


def test_create_and_drop_database_with_owner(database: Database):
    newDbName = create_random_db_name()
    database.create_database(newDbName, USER)
    assert database.does_db_exist(newDbName) is True
    database.drop_database(newDbName)
    assert database.does_db_exist(newDbName) is False


def test_create_destination_database(database: Database):
    destDbName = f"{DBNAME}_create_dest_test"
    try:
        database.create_destination_database(destDbName, drop=True, owner=None)
    except Exception as ex:
        pytest.fail("Could not copy Database")

    with pytest.raises(Exception):
        database.create_destination_database(destDbName, drop=False, owner=None)

    _try_drop_database(database, destDbName)


def test_create_destination_database_with_owner(database: Database):
    destDbName = f"{DBNAME}_create_dest_test"
    try:
        database.create_destination_database(destDbName, drop=True, owner=USER)
    except Exception as ex:
        pytest.fail("Could not copy Database")

    with pytest.raises(Exception):
        database.create_destination_database(destDbName, drop=False, owner=USER)

    _try_drop_database(database, destDbName)


def test_failed_database_connection():
    with pytest.raises(Exception):
        db = Database("test", "test", "no", "host123", 123)


def test_execute_with_error(database: Database):
    with pytest.raises(Exception):
        database._execute("SELECT * FROM not_a_table_name")

    with pytest.raises(Exception):
        database._execute_fetchall("SELECT * FROM not_a_table_name")

    with pytest.raises(Exception):
        database._execute_fetchone("SELECT * FROM not_a_table_name")


def test_drop_db_with_error(database: Database):
    with pytest.raises(Exception):
        database.drop_database(create_random_db_name())


def test_create_db_with_error(database: Database):
    new_db_name = create_random_db_name()
    database.create_database(new_db_name)
    with pytest.raises(Exception):
        database.create_database(new_db_name)
    database.drop_database(new_db_name)


def test_get_tables(database: Database):
    tables = database.get_tables()
    assert type(tables) is type([])


def test_get_relations(database: Database):
    constraints = database.get_relations()
    assert type(constraints) is type([])


def test_get_constraints(database: Database):
    constraints = database.get_constraints()
    assert type(constraints) is type([])


def test_get_realtions(database: Database):
    relations = database.get_relations()
    assert type(relations) is type([])


def test_get_column_infos(database: Database, table: TablePgInfo):
    column_infos = database.get_column_infos(table.name)
    assert type(column_infos) is type([])


def test_get_stats_for_table(database: Database, table: TablePgInfo):
    stats = database.get_stats(table.name)
    assert type(stats) is type([])


def test_get_stats_for_column(
    database: Database, table: TablePgInfo, column: ColumnPgInfo
):
    database.analyze()
    stats = database.get_stats(table.name, column.column_name)
    assert type(stats) is ColumnPgStats
    assert type(stats.histogram_bounds) == type([])


def test_get_column_comment(
    database: Database, table: TablePgInfo, column: ColumnPgInfo
):
    database.analyze()
    column_comment = database.get_column_comment(table.name, column.column_name)
    if column_comment:
        assert type(column_comment) is str


def test_run_statistic_queries(
    database: Database, table: TablePgInfo, column: ColumnPgInfo
):
    database.analyze()
    query_1 = "SELECT {column_name} FROM {table_name} LIMIT 1"
    result_1 = database.run_statistic_query(query_1, table.name, column.column_name)
    assert type(result_1) is type([])

    query_2 = "SELECT {column_name_str} as column_name, {table_name_str} as table_name"
    result_2 = database.run_statistic_query(query_2, table.name, column.column_name)
    assert type(result_2) is type([])
    assert len(result_2) == 1
    assert result_2[0]["column_name"] == column.column_name
    assert result_2[0]["table_name"] == table.name


def test_query_generator(database: Database):
    query, template = _create_insert_query(
        tablename="table1", colum_names=["column1", "column2"]
    )
    assert (
        query.as_string(database._conn)
        == 'INSERT INTO "table1" ("column1","column2") VALUES %s'
    )
    assert template.as_string(database._conn) == "(%(column1)s,%(column2)s)"


def _try_drop_database(database, dbName):
    try:
        database.drop_database(dbName)
    except Exception:
        pass


@pytest.mark.skipif(
    not HAS_POSTGRES_CLIENT_TOOLS, reason="postgres client tools not installed"
)
def test_dump_database(test_dump_file, database: Database):
    connectionStr = database.get_connection_url()
    assert dump_database(connectionStr, test_dump_file) == 0
    assert os.path.getsize(test_dump_file) > 0


@pytest.mark.skipif(
    not HAS_POSTGRES_CLIENT_TOOLS, reason="postgres client tools not installed"
)
def test_import_database(test_dump_file, database: Database):
    assert os.path.getsize(test_dump_file) > 0
    destDbName = f"{DBNAME}_test_import_database"
    database.create_destination_database(destDbName, drop=True)
    dest = Database(destDbName, USER, PW, HOST, PORT)
    connectionStr = dest.get_connection_url()
    assert import_database(connectionStr, test_dump_file) == 0
    del dest
    _try_drop_database(database, destDbName)


@pytest.mark.skipif(
    not HAS_POSTGRES_CLIENT_TOOLS, reason="postgres client tools not installed"
)
def test_copy_database(database: Database):
    destDbName = f"{DBNAME}_test_copy_database"
    database.create_destination_database(destDbName, drop=True)
    dest = Database(destDbName, USER, PW, HOST, PORT)
    copy_database(database, dest)
    del dest
    _try_drop_database(database, destDbName)
