import os
from subprocess import PIPE, Popen

from dotenv import load_dotenv

from pgsynthdata.database import Database

load_dotenv()

DBNAME = os.getenv("TEST_DBNAME")
USER = os.getenv("TEST_USER")
PW = os.getenv("TEST_PW")
HOST = os.getenv("TEST_HOST")
PORT = os.getenv("TEST_PORT")
PORT = os.getenv("TEST_PORT")


def _check_database_connection() -> bool:
    try:
        Database(DBNAME, USER, PW, HOST, PORT)
    except:
        return False
    return True


def _check_postgres_client_tools() -> bool:
    try:
        with Popen(
            [
                "pg_dump",
                "--help",
            ],
            stdout=PIPE,
        ) as process:
            process.wait()

        with Popen(
            [
                "pg_restore",
                "--help",
            ],
            stdout=PIPE,
        ) as process:
            process.wait()
        return True
    except FileNotFoundError:
        return False


HAS_POSTGRES_CLIENT_TOOLS = _check_postgres_client_tools()
CAN_CONNECT_TO_DATABASE = _check_database_connection()
