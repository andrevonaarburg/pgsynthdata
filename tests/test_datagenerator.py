from pgsynthdata.database import simple_column_pg_infos
from pgsynthdata.datagenerator import (
    FK_GENERATOR_NAME,
    Generator,
    StatisticQuery,
    _generators_tree,
    _get_from_generators_tree,
    _parse_comment,
    _register,
    empty_contraints,
    generator,
    get_generator,
)


def test_import_generators():
    # HINT this test passes, if any generator was found and imported by datagenerator._import_generators()
    assert len(_generators_tree) > 0


def test_fk_generator_is_present():
    pg_info = simple_column_pg_infos("doesn't matter")
    pg_constraints = empty_contraints()
    pg_constraints.is_fk = True

    assert (
        _get_from_generators_tree([None, FK_GENERATOR_NAME, None]) != None
    ), "FK-generator present in generator-dict"
    gen = get_generator(pg_info, pg_constraints)
    assert gen != None
    assert gen.function != None, "get_generator did not found FK-generator"


def test_register_and_get_function():
    # fixture
    def test_function():
        pass

    constraints = empty_contraints()
    test = simple_column_pg_infos("test_register_and_get_function")

    # exercise
    _register(
        "test_register_and_get_function", None, None, Generator(test_function, None)
    )

    # verify
    actual = get_generator(test, constraints)
    assert actual != None
    assert actual.function == test_function


def test_register_and_get_udt():
    # fixture
    test_function = len  # placeholder for a function
    constraints = empty_contraints()
    test_udt = simple_column_pg_infos("test_register_and_get_udt", "onlythis")
    test_wrongudt = simple_column_pg_infos("test_register_and_get_udt", "somethingelse")

    # exercise
    _register(
        "test_register_and_get_udt", "onlythis", None, Generator(test_function, None)
    )

    # verify
    assert get_generator(test_udt, constraints) != None
    assert get_generator(test_wrongudt, constraints) == None


def test_register_and_get_query():
    # setup
    test_function = print  # placeholder for a function
    constraints = empty_contraints()
    test = simple_column_pg_infos("test_register_and_get_query")
    test_query = StatisticQuery("myname", "SELECT something")

    # exercise
    _register(
        "test_register_and_get_query",
        None,
        None,
        Generator(test_function, custom_queries=[test_query]),
    )

    # verify
    actual = get_generator(test, constraints)
    assert actual != None
    assert actual.custom_queries.pop() == test_query


def test_register_and_get_comment_identifier():
    # fixture
    test_function = len  # placeholder for a function
    comment_identifier = "test_constraint"
    constraints = empty_contraints()
    test_column = simple_column_pg_infos("test_register_and_get_comment_identifier")
    test_column_with_udt = simple_column_pg_infos(
        "test_register_and_get_comment_identifier", "test_udt_name"
    )

    # exercise
    _register(
        "test_register_and_get_comment_identifier",
        udt_name=None,
        comment_identifier=comment_identifier,
        generator=Generator(test_function, None),
    )

    _register(
        "test_register_and_get_comment_identifier",
        udt_name="test_udt_name",
        comment_identifier=comment_identifier,
        generator=Generator(test_function, None),
    )

    # verify
    assert (
        get_generator(test_column, constraints, f"PGSYNTHDATA[{comment_identifier}]")
        != None
    )
    assert (
        get_generator(
            test_column,
            constraints,
            f"asdfa sd  PGSYNTHDATA[{comment_identifier}] asdfdf f asdf",
        )
        != None
    )
    assert (
        get_generator(
            test_column_with_udt,
            constraints,
            f"asdfa sd  PGSYNTHDATA[{comment_identifier}] asdfdf f asdf",
        )
        != None
    )


def test_register_and_get_comment_identifier_2():
    # fixture
    test_function = len  # placeholder for a function
    comment_identifier = "test_constraint"
    constraints = empty_contraints()
    test_column = simple_column_pg_infos("test_register_and_get_comment_identifier_2")
    test_column_with_udt = simple_column_pg_infos(
        "test_register_and_get_comment_identifier_2", "test_udt_name"
    )

    # exercise
    _register(
        None,
        udt_name={None, "test_udt_name"},
        comment_identifier=comment_identifier,
        generator=Generator(test_function, None),
    )

    # verify
    assert (
        get_generator(test_column, constraints, f"PGSYNTHDATA[{comment_identifier}]")
        != None
    )
    assert (
        get_generator(
            test_column_with_udt, constraints, f"PGSYNTHDATA[{comment_identifier}]"
        )
        != None
    )


def test_register_and_get_none():
    # setup
    constraints = empty_contraints()
    none = simple_column_pg_infos("test_register_and_get_none")

    # do
    # _register nothing

    # verify
    assert get_generator(none, constraints) == None


def test_generator_decorator_data_types():

    # setup
    pg_constraints = empty_contraints()

    # exercise
    @generator({"test_generator_decorator1", "test_generator_decorator2"})
    def test_generator():
        pass

    # verify
    actual1 = get_generator(
        simple_column_pg_infos("test_generator_decorator1"), pg_constraints
    )
    actual2 = get_generator(
        simple_column_pg_infos("test_generator_decorator2"), pg_constraints
    )
    actualxxx = get_generator(simple_column_pg_infos("xxx"), pg_constraints)

    assert actual1 != None and actual1.function == test_generator
    assert actual2 != None and actual1.function == test_generator
    assert actualxxx == None


def test_parse_comment():
    test = "NAME"
    match = _parse_comment(f"aasdf PGSYNTHDATA[{test}] asdfasdf")
    assert match == test
    match = _parse_comment(f"PGSYNTHDATA[{test}]")
    assert match == test
    match = _parse_comment(f"PGSYNTHDATA[{test}")
    assert match == None
    match = _parse_comment(f"PGSYNTHDATA[{test}] adf asf PGSYNTHDATA[{test}123]")
    assert match == test
    match = _parse_comment(None)
    assert match == None


def test_get_from_generator_tree():
    def f1():
        return 1

    def f2():
        return 2

    def f3():
        return 3

    def f4():
        return 4

    def f5():
        return 5

    def f6():
        return 6

    def f7():
        return 7

    def f8():
        return 8

    def f9():
        return 8

    _register("my_test_type", None, None, Generator(f1, None))
    _register("my_test_type", "my_test_udt", None, Generator(f2, None))
    _register("my_test_type", "my_test_udt", "test", Generator(f3, None))
    _register("my_test_type", None, "test", Generator(f4, None))
    _register(None, None, "test", Generator(f5, None))
    _register(None, "my_test_udt", "test", Generator(f6, None))
    _register(None, "my_test_udt", None, Generator(f7, None))
    _register("my_test_type_2", "my_test_udt_2", "test_2", Generator(f8, None))
    _register("my_test_type_2", "my_test_udt_2", None, Generator(f9, None))

    gen = _get_from_generators_tree([None, "my_test_type", None])
    assert isinstance(gen, Generator)
    assert gen.function == f1

    gen = _get_from_generators_tree(["unknown", "my_test_type", None])
    assert isinstance(gen, Generator)
    assert gen.function == f1

    gen = _get_from_generators_tree([None, "my_test_type", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f2

    gen = _get_from_generators_tree(["unknown", "my_test_type", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f2

    gen = _get_from_generators_tree(["test", "my_test_type", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f3

    gen = _get_from_generators_tree(["test", "my_test_type", None])
    assert isinstance(gen, Generator)
    assert gen.function == f4

    gen = _get_from_generators_tree(["test", "my_test_type", "unknown"])
    assert isinstance(gen, Generator)
    assert gen.function == f4

    gen = _get_from_generators_tree(["test", None, None])
    assert isinstance(gen, Generator)
    assert gen.function == f5

    gen = _get_from_generators_tree(["test", "unknown", None])
    assert isinstance(gen, Generator)
    assert gen.function == f5

    gen = _get_from_generators_tree(["test", None, "unknown"])
    assert isinstance(gen, Generator)
    assert gen.function == f5

    gen = _get_from_generators_tree(["test", "unknown", "unknown"])
    assert isinstance(gen, Generator)
    assert gen.function == f5

    gen = _get_from_generators_tree(["test", None, "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f6

    gen = _get_from_generators_tree(["test", "unknown", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f6

    gen = _get_from_generators_tree([None, None, "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f7

    gen = _get_from_generators_tree(["unknown", None, "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f7

    gen = _get_from_generators_tree([None, "unknown", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f7

    gen = _get_from_generators_tree(["unknown", "unknown", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f7

    # test backtrace
    gen = _get_from_generators_tree(["test_2", "my_test_type", None])
    assert isinstance(gen, Generator)
    assert gen.function == f1

    gen = _get_from_generators_tree(["test_2", "my_test_type", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f2

    gen = _get_from_generators_tree(["test_2", None, "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f7

    gen = _get_from_generators_tree([None, "my_test_type_2", "my_test_udt"])
    assert isinstance(gen, Generator)
    assert gen.function == f7

    # teset not found
    gen = _get_from_generators_tree(["test_2", "asaf", "asdf"])
    assert gen == None
