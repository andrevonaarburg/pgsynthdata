import pickle

from pgsynthdata.database import Database
from pgsynthdata.main import Config, Table, _get_database_structure
from tests.testhelpers import DBNAME, HOST, PORT, PW, USER

CONFIG = Config()
CONFIG.database = DBNAME
CONFIG.dest_database = DBNAME + "_GEN"
CONFIG.host = HOST
CONFIG.port = PORT
CONFIG.user = USER
CONFIG.m_factor = 1.0
CONFIG.tables = None
CONFIG.password = PW

OUT_PATH = "./tests/testdb.pickle"


def pickle_db(path=OUT_PATH):
    db = Database(
        CONFIG.database, CONFIG.user, CONFIG.password, CONFIG.host, CONFIG.port
    )
    tables = _get_database_structure(db, CONFIG)
    with open(path, "wb") as pickle_file:
        pickle.dump(tables, pickle_file)


def unpickle_db(path=OUT_PATH) -> list[Table]:
    try:
        with open(path, "rb") as pickle_file:
            return pickle.load(pickle_file)
    except Exception as ex:
        print(f"Error while trying to unpickle the testpickle '{path}': {ex}")


# HINT: run this file manually to update the pickle
if __name__ == "__main__":
    pickle_db()
    rick = unpickle_db()
    print("testdb-pickle successfully updated")
