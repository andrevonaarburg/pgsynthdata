import pytest
from toposort import CircularDependencyError

from pgsynthdata.database import Database, TablePgInfo, simple_column_pg_infos
from pgsynthdata.datagenerator import Generator, empty_contraints, empty_statistics
from pgsynthdata.generators.fk_generator import fk_generator
from pgsynthdata.generators.numeric_generator import numeric_generator
from pgsynthdata.main import (
    Column,
    Config,
    Table,
    _create_column_constraints,
    _generate_data,
    _get_database_structure,
    _toposort_and_filter_table_relations,
    analyze,
    generate,
    show,
)
from tests.testhelpers import (
    CAN_CONNECT_TO_DATABASE,
    DBNAME,
    HAS_POSTGRES_CLIENT_TOOLS,
    HOST,
    PORT,
    PW,
    USER,
)

DBNAME_DEST = f"{DBNAME}_gen"


@pytest.fixture
def config() -> Config:
    config = Config(database=DBNAME, dest_database=DBNAME_DEST)
    config.host = HOST
    config.port = PORT
    config.password = PW
    return config


@pytest.fixture
def database() -> Database:
    db = None
    try:
        db = Database(DBNAME, USER, PW, HOST, PORT)
    except Exception as ex:
        pytest.skip("can not connect to database")
    return db


def test_toposort():
    allTables = dict(
        table1=Table(pg_info=TablePgInfo(name="table1"), rows_to_generate=1),
        table2=Table(pg_info=TablePgInfo(name="table2"), rows_to_generate=1),
        table3=Table(pg_info=TablePgInfo(name="table3"), rows_to_generate=1),
        table4=Table(pg_info=TablePgInfo(name="table4"), rows_to_generate=1),
        table5=Table(pg_info=TablePgInfo(name="table5"), rows_to_generate=1),
        table6=Table(pg_info=TablePgInfo(name="table6"), rows_to_generate=1),
    )
    relations = [
        dict(tablename="table1", depends_on="{table2,table3}"),
        dict(tablename="table2", depends_on="{table5}"),
    ]
    actual = _toposort_and_filter_table_relations(relations, allTables)
    expected = [{"table3", "table4", "table5", "table6"}, {"table2"}, {"table1"}]
    assert len(actual) == len(expected)
    assert all([a == b for a, b in zip(actual, expected)])

    # empty filter
    actual2 = _toposort_and_filter_table_relations(relations, allTables, [])
    assert len(actual2) == len(expected)
    assert all([a == b for a, b in zip(actual2, expected)])


def test_toposort_filtered():
    allTables = dict(
        table1=Table(pg_info=TablePgInfo(name="table1"), rows_to_generate=1),
        table2=Table(pg_info=TablePgInfo(name="table2"), rows_to_generate=1),
        table3=Table(pg_info=TablePgInfo(name="table3"), rows_to_generate=1),
        table4=Table(pg_info=TablePgInfo(name="table4"), rows_to_generate=1),
        table5=Table(pg_info=TablePgInfo(name="table5"), rows_to_generate=1),
        table6=Table(pg_info=TablePgInfo(name="table6"), rows_to_generate=1),
    )
    relations = [
        dict(tablename="table1", depends_on="{table4,table3}"),
        dict(tablename="table2", depends_on="{table5}"),
    ]

    filter = ["table1", "table4", "table3"]

    actual = _toposort_and_filter_table_relations(relations, allTables, filter)
    expected = [{"table3", "table4"}, {"table1"}]
    assert len(actual) == len(expected)
    assert all([a == b for a, b in zip(actual, expected)])

    filter2 = ["table1", "table2"]

    with pytest.raises(Exception):
        _toposort_and_filter_table_relations(relations, allTables, filter2)


def test_toposort_circular():
    allTables = dict(
        table1=Table(pg_info=TablePgInfo(name="table1"), rows_to_generate=1),
        table2=Table(pg_info=TablePgInfo(name="table2"), rows_to_generate=1),
    )
    relations = [
        dict(tablename="table1", depends_on="{table2}"),
        dict(tablename="table2", depends_on="{table1}"),
    ]

    with pytest.raises(CircularDependencyError):
        _ = _toposort_and_filter_table_relations(relations, allTables)


def test_constraints_unique():
    constraints = [
        dict(
            foreign_column="",
            foreign_table="",
            referenced_primary_column="",
            referenced_primary_table="",
            constraint_type="",
            constraint_name="",
        ),
        dict(
            foreign_column="col1",
            foreign_table="table1",
            referenced_primary_column="col1",
            referenced_primary_table="table1",
            constraint_type="UNIQUE",
            constraint_name="unique1",
        ),
        dict(
            foreign_column="col2",
            foreign_table="table1",
            referenced_primary_column="col2",
            referenced_primary_table="table1",
            constraint_type="PRIMARY KEY",
            constraint_name="prim1",
        ),
        dict(
            foreign_column="col1",
            foreign_table="table2",
            referenced_primary_column="col2",
            referenced_primary_table="table1",
            constraint_type="FOREIGN KEY",
            constraint_name="for1",
        ),
        dict(
            foreign_column="col1",
            foreign_table="table3",
            referenced_primary_column="col2",
            referenced_primary_table="table1",
            constraint_type="FOREIGN KEY",
            constraint_name="for2",
        ),
    ]
    uniqueCol = _create_column_constraints(constraints, "table1", "col1")
    assert uniqueCol.is_unique
    assert len(uniqueCol.used_as_fk_by) == 0

    primaryCol = _create_column_constraints(constraints, "table1", "col2")
    expected = [("table2", "col1"), ("table3", "col1")]
    assert primaryCol.is_unique
    assert len(primaryCol.used_as_fk_by) >= 0
    assert all([a == b for a, b in zip(primaryCol.used_as_fk_by, expected)])

    foreignCol = _create_column_constraints(constraints, "table2", "col1")
    assert foreignCol.is_unique == False
    assert len(foreignCol.used_as_fk_by) == 0


@pytest.mark.skipif(not CAN_CONNECT_TO_DATABASE, reason="can not connect to database")
def test_show(config: Config):
    result = show(config)
    assert type(result) is dict


@pytest.mark.skipif(not CAN_CONNECT_TO_DATABASE, reason="can not connect to database")
@pytest.mark.skipif(not HAS_POSTGRES_CLIENT_TOOLS, reason="no postgresql client tools")
def test_generate(config: Config, database: Database):
    dest_db = f"{DBNAME_DEST}_pytest_generate"
    config.dest_database = dest_db
    config.drop == True
    generate(config)
    database.drop_database(dest_db)


@pytest.mark.skipif(not CAN_CONNECT_TO_DATABASE, reason="can not connect to database")
@pytest.mark.skipif(not HAS_POSTGRES_CLIENT_TOOLS, reason="no postgresql client tools")
def test_analyze(config: Config, database: Database):
    dest_db = f"{DBNAME_DEST}_pytest_analyze"
    config.drop = True
    config.dest_database = dest_db
    generate(config)
    res_output, res_fields = analyze(config)
    assert type(res_output) is dict
    assert type(res_fields) is list
    database.drop_database(dest_db)


def test_get_database_structure(config: Config, database: Database):
    tables = _get_database_structure(database, config)
    assert type(tables) is dict
    if len(tables) > 0:
        _, test_table = tables.popitem()
        assert type(test_table) is Table


def test_get_generate_data(config: Config, database: Database):
    tables = dict(
        table0=Table(
            pg_info=TablePgInfo(name="table0"),
            columns=dict(
                col1=Column(
                    pg_info=simple_column_pg_infos("integer"),
                    stats=empty_statistics(),
                    pg_comment=None,
                    generator=Generator(
                        function=numeric_generator, custom_queries=None
                    ),
                    constraints=empty_contraints(),
                )
            ),
            rows_to_generate=1,
        ),
        table1=Table(
            pg_info=TablePgInfo(name="table1"),
            columns=dict(
                col1=Column(
                    pg_info=simple_column_pg_infos("integer"),
                    stats=empty_statistics(),
                    pg_comment=None,
                    generator=Generator(
                        function=numeric_generator, custom_queries=None
                    ),
                    constraints=empty_contraints(),
                )
            ),
            rows_to_generate=1,
        ),
        table2=Table(
            pg_info=TablePgInfo(name="table2"),
            columns=dict(
                col1=Column(
                    pg_info=simple_column_pg_infos("integer"),
                    stats=empty_statistics(),
                    pg_comment=None,
                    generator=Generator(function=fk_generator, custom_queries=None),
                    constraints=empty_contraints(),
                )
            ),
            rows_to_generate=1,
        ),
    )

    tables["table0"].columns["col1"].stats.custom_stats = dict(
        absolut_min=[[1]], absolut_max=[[10000]]
    )
    tables["table1"].columns["col1"].stats.custom_stats = dict(
        absolut_min=[[1]], absolut_max=[[10000]]
    )
    tables["table2"].columns["col1"].stats.custom_stats = dict(
        absolut_min=[[1]], absolut_max=[[10000]]
    )
    tables["table0"].columns["col1"].constraints.used_as_fk_by = []
    tables["table1"].columns["col1"].constraints.used_as_fk_by = [("table2", "col1")]
    tables["table1"].columns["col1"].constraints.is_unique = True
    tables["table2"].columns["col1"].constraints.used_as_fk_by = []
    tables["table2"].columns["col1"].constraints.is_fk = True

    generated0 = _generate_data(tables, "table0")
    assert generated0["col1"][0]
    assert type(generated0["col1"][0]) == int

    generated1 = _generate_data(tables, "table1")
    assert generated1["col1"][0]
    assert type(generated1["col1"][0]) == int

    generated2 = _generate_data(tables, "table2")
    assert generated2["col1"][0]
    assert type(generated2["col1"][0]) == int
    assert generated2["col1"][0] == generated1["col1"][0]
